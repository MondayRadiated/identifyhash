#!/usr/bin/env python3
# encoding: utf-8


def CRC16():
    if (len(hash) == 4 and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("CRC-16")


def hash4fft():
    if (len(hash) == 4 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("CRC-16-CCITT")
        result.append("FCS-16")


def hash8fft():
    if (len(hash) == 8 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("CRC-32")
        result.append("ADLER-32")
        result.append("CRC-32B")
        result.append("XOR-32")


def GHash32x():
    if (len(hash) == 8 and
            hash.isdigit() is True and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("GHash-32-3")
        result.append("GHash-32-5")


def DESUnix():
    if (len(hash) == 13 and
            hash.isdigit() is False and
            hash.isalpha() is False):
        result.append("DES(Unix)")


def hash16():
    if (len(hash) == 16 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("MD5(Half)")
        result.append("MD5(Middle)")
        result.append("MySQL")


def hash32():
    if (len(hash) == 32 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("Haval-128")
        result.append("Domain Cached Credentials - MD4(MD4(($pass)).(strtolower($username)))")
        result.append("Haval-128(HMAC)")
        result.append("MD2")
        result.append("MD4")
        result.append("MD5")
        result.append("MD5(HMAC(Wordpress))")
        result.append("NTLM")
        result.append("RAdmin v2.x")
        result.append("RipeMD-128")
        result.append("RipeMD-128(HMAC)")
        result.append("SNEFRU-128")
        result.append("SNEFRU-128(HMAC)")
        result.append("Tiger-128")
        result.append("md5 with salt")


def MD5phpBB3():
    if (len(hash) == 34 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:3].find('$H$') == 0):
        result.append("MD5(phpBB3)")


def MD5Unix():
    if (len(hash) == 34 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:3].find('$1$') == 0):
        result.append("MD5(Unix)")


def MD5Wordpress():
    if (len(hash) == 34 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:3].find('$P$') == 0):
        result.append("MD5(Wordpress)")


def MD5APR():
    if (len(hash) == 37 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash[0:4].find('$apr') == 0):
        result.append("MD5(APR)")


def hash40():
    if (len(hash) == 40 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("Haval-160")
        result.append("MySQL5 - SHA-1(SHA-1($pass))")
        result.append("RipeMD-160")
        result.append("SHA-1 or SHA-1 with salt")
        result.append("Tiger-160")


def MySQL160bit():
    if (len(hash) == 41 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:1].find('*') == 0):
        result.append("MySQL 160bit - SHA-1(SHA-1($pass))")


def Haval192():
    if (len(hash) == 48 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("Haval-192")
        result.append("Tiger-192")


def MD5passsaltjoomla1():
    if (len(hash) == 49 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[32:33].find(':') == 0):
        result.append("md5($pass.$salt) - Joomla")


def SHA1Django():
    if (len(hash) == 52 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:5].find('sha1$') == 0):
        result.append("SHA-1(Django)")


def hash56():
    if (len(hash) == 56 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("Haval-224")
        result.append("SHA-224")


def hash64():
    if (len(hash) == 64 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("SHA-256")
        result.append("Haval-256")
        result.append("GOST R 34.11-94")
        result.append("RipeMD-256")
        result.append("SNEFRU-256")


def MD5passsaltjoomla2():
    if (len(hash) == 65 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[32:33].find(':') == 0):
        result.append("md5($pass.$salt) - Joomla")


def SAM():
    if (len(hash) == 65 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash.islower() is False and
            hash[32:33].find(':') == 0):
        result.append("SAM - (LM_hash:NT_hash)")


def SHA256Django():
    if (len(hash) == 78 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:6].find('sha256') == 0):
        result.append("SHA-256(Django)")


def RipeMD320():
    if (len(hash) == 80 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("RipeMD-320")


def SHA384():
    if (len(hash) == 96 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("SHA-384")


def SHA256s():
    if (len(hash) == 98 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:3].find('$6$') == 0):
        result.append("SHA-256")


def SHA384Django():
    if (len(hash) == 110 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is False and
            hash[0:6].find('sha384') == 0):
        result.append("SHA-384(Django)")


def SHA512():
    if (len(hash) == 128 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("SHA-512")


def Whirlpool():
    if (len(hash) == 128 and
            hash.isdigit() is False and
            hash.isalpha() is False and
            hash.isalnum() is True):
        result.append("Whirlpool")


while True:
    result = []
    hash = input('\n Enter HASH(or q to quit): ')
    if hash == 'q':
        exit()
    else:
        CRC16()
        hash4fft()
        hash8fft()
        hash16()
        DESUnix()
        hash32()
        GHash32x()
        hash40()
        hash56()
        hash64()
        Haval192()
        MD5APR()
        MD5phpBB3()
        MD5Unix()
        MD5Wordpress()
        MD5passsaltjoomla1()
        MD5passsaltjoomla2()
        MySQL160bit()
        RipeMD320()
        SAM()
        SHA1Django()
        SHA256s()
        SHA256Django()
        SHA384()
        SHA384Django()
        SHA512()
        Whirlpool()

        if len(result) == 0:
            print("\n Not Found.")
        else:
            result.sort()
            print("\n Possible Hashes:")
            for a in range(len(result)):
                print("[+] ", result[a])
